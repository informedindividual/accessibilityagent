var libs = {
    util: require('/lib/enonic/util/util'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
};
exports.get = function(req) {

    var component = libs.portal.getComponent();

    var model = {
        component: component,
        centerRegion: component.regions["center"]
    }

    var view = resolve('layout-1-col.html');
    var html = libs.thymeleaf.render(view, model);

    return{
        body: html,
        contentType: 'text/html'
    }

};