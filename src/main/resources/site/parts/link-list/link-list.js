var libs = {
    util: require('/lib/enonic/util/util'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    content: require('/lib/xp/content'),
    codeblock:require('codeblock/codeblock'),
    guideline: require('guideline/guideline')
};

exports.get = function(portal) {
    var component = libs.portal.getComponent();
    var links = component.config["link"] || [];

    var params = {
        component: component,
        links: links
    };
    var html =  libs.thymeleaf.render(resolve('link-list.html'), params);

    return {
        body: html,
        contentType: 'text/html'
    };

};