var libs = {
    util: require('/lib/enonic/util/util'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    content: require('/lib/xp/content'),
    codeblock:require('codeblock/codeblock'),
    guideline: require('guideline/guideline'),
    menu: require('/lib/enonic/menu/menu'),
    menutmp: require('util/js/menu')

};

exports.get = handleGet;
exports.post = handleGet;

function handleGet(req) {
    var model = getModel(req);
    var view = resolve('defaultpage.html');
    var html = libs.thymeleaf.render(view, model);

    return{
        body: html,
        contentType: 'text/html'
    }
}


function getModel(req){
    var site = libs.portal.getSite();
    var content = libs.portal.getContent();
    var siteConfig = libs.portal.getSiteConfig();
    var menuItems = libs.menutmp.getMenuTree(3);

    return {
        mainRegion: content.page.regions["main"],
        sitePath: site['_path'],
        siteConfig: siteConfig,
        menuItems: menuItems,
        path: content['_path'],
        editMode: req.mode === 'edit',
        req: req,
        site: site,
        content: content
    }
}