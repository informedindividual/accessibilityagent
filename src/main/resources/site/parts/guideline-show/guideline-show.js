var libs = {
    util: require('/lib/enonic/util/util'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    content: require('/lib/xp/content'),
    codeblock:require('codeblock/codeblock'),
    guideline: require('guideline/guideline')
};

function handleGet(req) {

    var result = libs.portal.getContent();
    var guideline = libs.guideline.getGuideline(result);

    var params = {
        req: req,
        guideline: guideline
    };

    return libs.guideline.renderGuideline(params);
}

exports.get = handleGet;