var libs = {
    util: require('/lib/enonic/util/util'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    content: require('/lib/xp/content'),
    codeblock:require('codeblock/codeblock'),
    jsonpath: require('util/js/jsonpath')
};


exports.getGuideline = function(element){

    var paragraphs = [];
    libs.jsonpath.process(element,"$..paragraphs")
        .forEach(getParagraphs, {"paragraphs":paragraphs});

    if (paragraphs){
        paragraphs.forEach(getParagraphCodeblocks);
    }

    element.data.paragraphs = paragraphs;

    return element;
}

//@param guideline (required) - guideline to be rendered
exports.renderGuideline = function(params){
    var view = resolve('/parts/guideline-show/guideline-show.html');
    return {
        body: libs.thymeleaf.render(view, params)};
}

function getParagraphs(element, index, array){
    if (Array.isArray(element)){
        element.forEach(getParagraphs, {"paragraphs":this.paragraphs});
    }else{
        this.paragraphs.push(element);
    }
}

function getParagraphCodeblocks(element, index, array){
    if (Array.isArray(element)){
        element.forEach(getParagraphs, {"paragraphs":this.paragraphs});
    }else{
        var codeblocks = [];
        libs.jsonpath.process(element,"$..codeblocks")
            .forEach(getCodeblocks,{"codeblocks":codeblocks})
        element.codeblocks = codeblocks;
    }
}

function getCodeblocks(element,index,array){
    if (Array.isArray(element)){
        element.forEach(getCodeblocks, {"codeblocks":this.codeblocks});
    }else{
        var codeblock = libs.codeblock.getCodeblocksFromKey(element);
        var renderedCodeblock = libs.codeblock.renderCodeblocks(codeblock, {"codeblocks":codeblock});
        this.codeblocks.push(renderedCodeblock);
    }
}
