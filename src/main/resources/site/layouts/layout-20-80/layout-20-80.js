var libs = {
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf')
};

exports.get = function(req) {

    var component = libs.portal.getComponent();

    var params = {
        component: component,
        leftRegion: component.regions["left"],
        rightRegion: component.regions["right"]
    }
    return libs.thymeleaf.render(resolve('layout-20-80.html'), params);

};