var libs = {
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content'),
    thymeleaf: require('/lib/xp/thymeleaf')
};

exports.get = handleGet;
exports.post = handleGet;

function handleGet(req) {

    // Get the content that is using the page
    var content = libs.portal.getContent();

    // Extract the main region which contains component parts
    var mainRegion = content.page.regions.main;

    // Prepare the model that will be passed to the view
    var model = {
        mainRegion: mainRegion
    };

    var view = resolve('voidpage.html');
    var html = libs.thymeleaf.render(view, model);

    return {
        body: html,
        contentType: 'text/html'
    }
}