var libs = {
    util: require('/lib/enonic/util/util'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    content: require('/lib/xp/content'),
    codeblock:require('codeblock/codeblock'),
    guideline: require('guideline/guideline'),
    cacheLib: require("/lib/xp/cache")
};

var cache = libs.cacheLib.newCache({
        size: 100,
        expire: 60*10
    }
);

var count = 0;


function handleGet(req) {

    var component = libs.portal.getComponent();
    var result = libs.portal.getContent();
    var codeblocks = libs.codeblock.getCodeblocks(result);

    var params = {
        context: req,
        component: component,
        codeblocks : codeblocks
    }

    try{
        var cachekey;
        codeblocks.forEach(function(child){
            cachekey = child._id+child.modifiedTime;
        });
        //log.info('My JSON %s', codeblocks );
        var result = cache.get(cachekey, function(){
            return libs.codeblock.renderCodeblocks(codeblocks, params);
        });
        return result;
    }catch (e){
        log.info("Exception: " + e);
        codeblocks.errorMessage = e.message;
    }
}

exports.get = handleGet;