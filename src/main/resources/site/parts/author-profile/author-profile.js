var libs = {
    util: require('/lib/enonic/util/util'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    content: require('/lib/xp/content'),
};
// Handle GET requests
function handleGet(req) {

    // Find the current component from request
    var component = libs.portal.getComponent();

    // Find a config variable for the component
    var author = component.config["author"] || [];
    var picture = component.config["picture"] || [];
    var subheader = component.config["subheader"] || [];
    var about = component.config["about"] || [];
    var linkedin = component.config["linkedin"];
    var facebook = component.config["facebook"];
    var github = component.config["github"];
    var twitter = component.config["twitter"];

    // Define the model
    var model = {
        component: component,
        author: author,
        picture:picture,
        subheader:subheader,
        about:about,
        linkedin:linkedin,
        facebook:facebook,
        github:github,
        twitter:twitter

    };

    // Resolve the view
    var view = resolve('author-profile.html');

    // Render a thymeleaf template
    var body = libs.thymeleaf.render(view, model);

    // Return the result
    return {
        body: body,
        contentType: 'text/html'
    };

};

exports.get = handleGet;