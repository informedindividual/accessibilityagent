/**
 * JRuby related functions.
 */

var libs = {
    util: require('/lib/enonic/util/util'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    content: require('/lib/xp/content'),
    portal: require('/lib/xp/portal')
};

var codeRayService = __.newBean("openxp.coderay.CoderayService");

exports.highlightCodeblock = function(codeblock, parentElement){

    var site = libs.portal.getSite();
    var defaultConfig = libs.portal.getSiteConfig();
    if (codeblock.code){
        var codeRay;
        try{
            if (codeblock.config == null){
                codeblock.config = defaultConfig;
            }
            codeRayService.code = codeblock.code;
            codeRayService.codeType = codeblock.codetype!=null?codeblock.codetype:defaultConfig.codetype;
            codeRayService.lineNumbers = codeblock.config.lineNumbers!=null?codeblock.config.lineNumbers:defaultConfig.lineNumbers;
            codeRayService.css = codeblock.config.css!=null?codeblock.config.css:defaultConfig.css;
            codeRayService.wrap = codeblock.config.wrap!=null?codeblock.config.wrap:defaultConfig.wrap;
            codeRayService.tabWidth = codeblock.config.tabWidth!=null?codeblock.config.tabWidth:defaultConfig.tabWidth;
            codeRayService.hint = codeblock.config.hint!=null?codeblock.config.hint:defaultConfig.hint;
            codeRayService.lineNumberAnchors = codeblock.config.lineNumberAnchors!=null?codeblock.config.lineNumberAnchors:defaultConfig.lineNumberAnchors;
            codeRayService.lineNumberAnchorsPrefix = codeblock.config.lineNumberAnchorsPrefix!=""?codeblock.config.lineNumberAnchorsPrefix:defaultConfig.lineNumberAnchorsPrefix;
            codeRayService.breakLines = codeblock.config.breakLines!=null?codeblock.config.breakLines:defaultConfig.breakLines;
            codeRayService.highlightLines = codeblock.config.highlightLines;
            codeRayService.modifiedTime = parentElement.modifiedTime;
            codeRayService.parentElement = parentElement;

            codeRay = codeRayService.highlightCode();
        }catch (e){
            log.info("Exception when highlighting code: " + e);
            codeblock.errorMessage = e.message;
        }
        codeblock.codeRay = codeRay;
    }
};
