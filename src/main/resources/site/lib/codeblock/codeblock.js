var libs = {
    util: require('/lib/enonic/util/util'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    content: require('/lib/xp/content'),
    jsonpath: require('util/js/jsonpath'),
    codeRay: require('/lib/jruby/coderay')
};

var codeblocks = [];

exports.getCodeblocksFromKey = function(element){
    var codeblocksResult = libs.content.get({
        key: element
    });
    return exports.getCodeblocks(codeblocksResult);

}

exports.getCodeblocks = function(element){
    var outerCodeblocks = [];
    libs.jsonpath.process(element,"$")
        .forEach(getOuterCodeblocks, {"outerCodeblocks":outerCodeblocks});
    return outerCodeblocks;
}

function getOuterCodeblocks(element,index,array){
    var innerCodeblocks = [];
    libs.jsonpath.process(element,"$..codeblock")
        .forEach(getInnerCodeblocks, {"innerCodeblocks":innerCodeblocks,parentElement:element});
    element.data.codeblock = innerCodeblocks;
    this.outerCodeblocks.push(element);
}

function getInnerCodeblocks(element, index, array){
    if (Array.isArray(element)){
        element.forEach(getInnerCodeblocks, {"innerCodeblocks":this.innerCodeblocks, parentElement:this.parentElement});
    }else{
        formatCodeblock(element, this.parentElement);
        this.innerCodeblocks.push(element);
    }
}

function formatCodeblock(element, parentElement){
    //format codeblock with codeRay
    libs.codeRay.highlightCodeblock(element, parentElement);

    //Get codecomments
    var codecomments = [];
    libs.jsonpath.process(element, "$..codecomment")
        .forEach(getCodecomments, {"codecomments":codecomments});
    element.codecomment = codecomments;
}

function getCodecomments(element, index, array){
    if (Array.isArray(element)){
        element.forEach(getCodecomments, {"codecomments":this.codecomments});
    }else{
        this.codecomments.push(element);
    }
}

exports.renderCodeblocks = function(codeblocks, params){
    var view = resolve('/parts/codeblock-show/codeblock-show.html');
    return {body: libs.thymeleaf.render(view, params)};
}


