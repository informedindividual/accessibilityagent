var libs = {
    util: require('/lib/enonic/util/util'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    content: require('/lib/xp/content'),
    codeblock:require('codeblock/codeblock'),
    guideline: require('guideline/guideline'),
    jsonpath: require('util/js/jsonpath')
};

exports.get = function(req) {
    var site = libs.portal.getSite();
    var component = libs.portal.getComponent();
    var config = component.config;
    var result = libs.portal.getContent();

    var guidelinesResult = libs.content.getChildren({
        key: result._path,
        start: 0,
        count: 50
    });

    var guidelines = [];



    libs.jsonpath.process(guidelinesResult,"$..hits")
        .forEach(getGuidelines, {"guidelines":guidelines});

    var params = {
        component: component,
        guidelines: guidelines
    };

    var html = libs.thymeleaf.render(resolve('guideline-list.html'), params);

    return{
        body: html,
        contentType: 'text/html'
    }
};

function getGuidelines(element, index, array){
    if (Array.isArray(element)){
        element.forEach(getGuidelines, {"guidelines":this.guidelines});
    }else{
        var guideline = libs.guideline.getGuideline(element);
        var renderedGuideline = libs.guideline.renderGuideline({"guideline":guideline});
        this.guidelines.push(renderedGuideline);
    }
}
