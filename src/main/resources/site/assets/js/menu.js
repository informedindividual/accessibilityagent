window.onload = (function() {

    //http://foundation.zurb.com/docs/components/offcanvas.html
    var menuConfig = {
        leftOffCanvasToggleClass: '.left-off-canvas-toggle',
        leftOffCanvasMenuClass: '.left-off-canvas-menu',
        leftOffCanvasMenuNoJsClass: '.nojs-left-off-canvas-menu',
        exitOffCanvasClass:'.exit-off-canvas',
        offCanvasWrapClass:'.off-canvas-wrap'
    }

    var accessibility = {
        skipToContentClass:'.skip-to-content',
        mainID:'#main',
        showWhenJsClass:'.show-when-js',
        hideWhenJsClass:'.hide-when-js'
    }

    var showWhenJs = document.querySelectorAll(accessibility.showWhenJsClass);
    var hideWhenJs = document.querySelectorAll(accessibility.hideWhenJsClass);

    if (showWhenJs){
        [].forEach.call(showWhenJs, function(el) {
            el.removeAttribute("aria-hidden");
        });
    }
    if (hideWhenJs) {
        [].forEach.call(hideWhenJs, function (el) {
            el.remove();
        });
    }

    //Change the no javascript menu into a fancy off-canvas javascript menu.
    var nojsMenu = document.querySelector(menuConfig.leftOffCanvasMenuNoJsClass);
    nojsMenu.className = menuConfig.leftOffCanvasMenuClass.substr(1);
    nojsMenu.setAttribute("aria-hidden",true);

    //Handle aria attributes, focus and accessebility when using/skipping menu
    var leftMenuIcon = document.querySelector(menuConfig.leftOffCanvasToggleClass);
    var leftMenu = document.querySelector(menuConfig.leftOffCanvasMenuClass);
    var exitOffCanvas = document.querySelector(menuConfig.exitOffCanvasClass);
    var skipToContent = document.querySelector(accessibility.skipToContentClass);

    if (skipToContent) {
        skipToContent.addEventListener('click', function () {
            leftMenu.setAttribute("aria-hidden", true);
            leftMenuIcon.setAttribute("aria-expanded", false);
            exitOffCanvas.click();
            var main = document.querySelector(accessibility.mainID);
            main.focus();
        }, false);
    }
    if (leftMenuIcon){
        leftMenuIcon.addEventListener('click', function(){
            leftMenu.setAttribute("aria-hidden",false);
            leftMenuIcon.setAttribute("aria-expanded",true);
            leftMenu.querySelector('a:first-of-type').focus();
        }, false);
    }

    if (leftMenu){
        leftMenu.addEventListener('click', function(){
            leftMenu.setAttribute("aria-hidden",true);
        }, false);
    }
    if (exitOffCanvas){
        exitOffCanvas.addEventListener('click', function(){
            leftMenu.setAttribute("aria-hidden",true);
            leftMenuIcon.setAttribute("aria-expanded",false);
        }, false);
    }

})();









